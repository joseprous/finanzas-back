{-# LANGUAGE PackageImports #-}
import "finanzas-back" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
