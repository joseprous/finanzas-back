module Handler.User where

import Data.Text.Encoding as TE
import Database.Persist.Sql (fromSqlKey, toSqlKey)
import Handler.Common
import Import

getUsersR :: Handler Value
getUsersR = do
  users <- runDB $ selectList [] [Asc UserNombre]
  sendStatusJSON ok200 users

postUsersR :: Handler ()
postUsersR = do
  msg <- requireCheckJsonBody :: Handler User
  mPasswordHash <- liftIO $ hashText $ userPassword msg
  case mPasswordHash of
    Nothing -> sendBadRequest
    Just passwordHash -> do
      let user = msg {userPassword = TE.decodeUtf8 passwordHash}
      pid <- runDB $ insert user
      sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getUserR :: Int64 -> Handler Value
getUserR n = do
  let key = toSqlKey n :: Key User
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteUserR :: Int64 -> Handler ()
deleteUserR n = do
  let k = toSqlKey n :: Key User
  runDB $ deleteCascade $ k
  sendDeleted
