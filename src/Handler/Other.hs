module Handler.Other where

import Database.Persist.Sql (fromSqlKey, toSqlKey)
import Handler.Common
import Import

getCondicionesVentaR :: Handler Value
getCondicionesVentaR = do
  condicionesVenta <- runDB $ selectList [] [Asc CondicionVentaNombre]
  sendStatusJSON ok200 condicionesVenta

postCondicionesVentaR :: Handler ()
postCondicionesVentaR = do
  msg <- requireCheckJsonBody :: Handler CondicionVenta
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getCondicionVentaR :: Int64 -> Handler Value
getCondicionVentaR n = do
  let key = toSqlKey n :: Key CondicionVenta
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteCondicionVentaR :: Int64 -> Handler ()
deleteCondicionVentaR n = do
  let k = toSqlKey n :: Key CondicionVenta
  runDB $ deleteCascade $ k
  sendDeleted

getTiposIdentificacionR :: Handler Value
getTiposIdentificacionR = do
  tiposIdentificacion <- runDB $ selectList [] [Asc TipoIdentificacionNombre]
  sendStatusJSON ok200 tiposIdentificacion

postTiposIdentificacionR :: Handler ()
postTiposIdentificacionR = do
  msg <- requireCheckJsonBody :: Handler TipoIdentificacion
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getTipoIdentificacionR :: Int64 -> Handler Value
getTipoIdentificacionR n = do
  let key = toSqlKey n :: Key TipoIdentificacion
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteTipoIdentificacionR :: Int64 -> Handler ()
deleteTipoIdentificacionR n = do
  let k = toSqlKey n :: Key TipoIdentificacion
  runDB $ deleteCascade $ k
  sendDeleted
