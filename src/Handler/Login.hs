module Handler.Login where

import Crypto.BCrypt
import Data.Map as Map
import Data.Text.Encoding as TE
import Database.Persist.Sql (fromSqlKey, toSqlKey)
import Handler.Common
import Import
import Web.JWT

data Login
  = Login
      { username :: Text,
        password :: Text
      }
  deriving (Generic)

instance FromJSON Login

instance ToJSON Login

encodeJWT :: Entity User -> Text -> Text
encodeJWT (Entity k user) secret =
  let cs =
        mempty
          { sub = stringOrURI $ tshow $ fromSqlKey k,
            unregisteredClaims =
              ClaimsMap $ Map.fromList [("username", String $ userUsername user)]
          }
      key = hmacSecret secret
   in encodeSigned key mempty cs

postLoginR :: Handler ()
postLoginR = do
  app <- getYesod
  let settings = appSettings app
  let secret = appJWTSecret settings
  msg <- requireCheckJsonBody :: Handler Login
  maybeUser <- runDB $ getBy $ UniqueUsername $ username msg
  $logDebug $ tshow maybeUser
  case maybeUser of
    Nothing -> sendUnauthorized
    Just eu@(Entity k user) -> do
      let postPassword = TE.encodeUtf8 $ password msg
      let dbPassword = TE.encodeUtf8 $ userPassword user
      if validatePassword dbPassword postPassword
        then
          sendStatusJSON ok200 $
            object
              [ "token" .= encodeJWT eu secret,
                "userId" .= fromSqlKey k
              ]
        else sendUnauthorized
