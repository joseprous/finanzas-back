module Handler.Ingresos where

import Data.Text.Read
import Database.Persist.Sql (fromSqlKey, toSqlKey)
import Handler.Common
import Import

getTiposDocumentoIngresoR :: Handler Value
getTiposDocumentoIngresoR = do
  tiposDocumento <- runDB $ selectList [] [Asc TipoDocumentoIngresoNombre]
  sendStatusJSON ok200 tiposDocumento

postTiposDocumentoIngresoR :: Handler ()
postTiposDocumentoIngresoR = do
  msg <- requireCheckJsonBody :: Handler TipoDocumentoIngreso
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getTipoDocumentoIngresoR :: Int64 -> Handler Value
getTipoDocumentoIngresoR n = do
  let key = toSqlKey n :: Key TipoDocumentoIngreso
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteTipoDocumentoIngresoR :: Int64 -> Handler ()
deleteTipoDocumentoIngresoR n = do
  let k = toSqlKey n :: Key TipoDocumentoIngreso
  runDB $ deleteCascade $ k
  sendDeleted

getTiposIngresoR :: Handler Value
getTiposIngresoR = do
  tiposIngreso <- runDB $ selectList [] [Asc TipoIngresoNombre]
  sendStatusJSON ok200 tiposIngreso

postTiposIngresoR :: Handler ()
postTiposIngresoR = do
  msg <- requireCheckJsonBody :: Handler TipoIngreso
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getTipoIngresoR :: Int64 -> Handler Value
getTipoIngresoR n = do
  let key = toSqlKey n :: Key TipoIngreso
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteTipoIngresoR :: Int64 -> Handler ()
deleteTipoIngresoR n = do
  let k = toSqlKey n :: Key TipoIngreso
  runDB $ deleteCascade $ k
  sendDeleted

getLimitFilter :: Maybe Text -> [SelectOpt record]
getLimitFilter mLimit =
  case mLimit of
    Nothing -> []
    Just tlimit ->
      case decimal tlimit of
        Right (limit, _) -> [LimitTo limit]
        _ -> []

getIngresosR :: Handler Value
getIngresosR = do
  mLimit <- lookupGetParam "limit"
  let limitFilter = getLimitFilter mLimit
  muserKey <- cacheGet
  case muserKey of
    Just (CachedUserKey (Just userKey)) -> do
      ingresos <- runDB $ selectList [IngresoUserId ==. userKey] ([Asc IngresoNombre] ++ limitFilter)
      sendStatusJSON ok200 ingresos
    _ -> sendNotFound

postIngresosR :: Handler ()
postIngresosR = do
  msg <- requireCheckJsonBody :: Handler Ingreso
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getIngresoR :: Int64 -> Handler Value
getIngresoR n = do
  let key = toSqlKey n :: Key Ingreso
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteIngresoR :: Int64 -> Handler ()
deleteIngresoR n = do
  let k = toSqlKey n :: Key Ingreso
  runDB $ deleteCascade $ k
  sendDeleted
