module Handler.Egresos where

import Data.Text.Read
import Database.Persist.Sql (fromSqlKey, toSqlKey)
import Handler.Common
import Import

getCategoriasR :: Handler Value
getCategoriasR = do
  categorias <- runDB $ selectList [] [Asc CategoriaNombre]
  sendStatusJSON ok200 categorias

postCategoriasR :: Handler ()
postCategoriasR = do
  msg <- requireCheckJsonBody :: Handler Categoria
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getCategoriaR :: Int64 -> Handler Value
getCategoriaR n = do
  let key = toSqlKey n :: Key Categoria
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteCategoriaR :: Int64 -> Handler ()
deleteCategoriaR n = do
  let k = toSqlKey n :: Key Categoria
  runDB $ deleteCascade $ k
  sendDeleted

getTiposDocumentoEgresoR :: Handler Value
getTiposDocumentoEgresoR = do
  tiposDocumento <- runDB $ selectList [] [Asc TipoDocumentoEgresoNombre]
  sendStatusJSON ok200 tiposDocumento

postTiposDocumentoEgresoR :: Handler ()
postTiposDocumentoEgresoR = do
  msg <- requireCheckJsonBody :: Handler TipoDocumentoEgreso
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getTipoDocumentoEgresoR :: Int64 -> Handler Value
getTipoDocumentoEgresoR n = do
  let key = toSqlKey n :: Key TipoDocumentoEgreso
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteTipoDocumentoEgresoR :: Int64 -> Handler ()
deleteTipoDocumentoEgresoR n = do
  let k = toSqlKey n :: Key TipoDocumentoEgreso
  runDB $ deleteCascade $ k
  sendDeleted

getTiposEgresoR :: Handler Value
getTiposEgresoR = do
  tiposEgreso <- runDB $ selectList [] [Asc TipoEgresoNombre]
  sendStatusJSON ok200 tiposEgreso

postTiposEgresoR :: Handler ()
postTiposEgresoR = do
  msg <- requireCheckJsonBody :: Handler TipoEgreso
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getTipoEgresoR :: Int64 -> Handler Value
getTipoEgresoR n = do
  let key = toSqlKey n :: Key TipoEgreso
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteTipoEgresoR :: Int64 -> Handler ()
deleteTipoEgresoR n = do
  let k = toSqlKey n :: Key TipoEgreso
  runDB $ deleteCascade $ k
  sendDeleted

getClasificacionesEgresoR :: Handler Value
getClasificacionesEgresoR = do
  clasificacionesEgreso <- runDB $ selectList [] [Asc ClasificacionEgresoNombre]
  sendStatusJSON ok200 clasificacionesEgreso

postClasificacionesEgresoR :: Handler ()
postClasificacionesEgresoR = do
  msg <- requireCheckJsonBody :: Handler ClasificacionEgreso
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getClasificacionEgresoR :: Int64 -> Handler Value
getClasificacionEgresoR n = do
  let key = toSqlKey n :: Key ClasificacionEgreso
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteClasificacionEgresoR :: Int64 -> Handler ()
deleteClasificacionEgresoR n = do
  let k = toSqlKey n :: Key ClasificacionEgreso
  runDB $ deleteCascade $ k
  sendDeleted

getLimitFilter :: Maybe Text -> [SelectOpt record]
getLimitFilter mLimit =
  case mLimit of
    Nothing -> []
    Just tlimit ->
      case decimal tlimit of
        Right (limit, _) -> [LimitTo limit]
        _ -> []

getEgresosR :: Handler Value
getEgresosR = do
  mLimit <- lookupGetParam "limit"
  let limitFilter = getLimitFilter mLimit
  muserKey <- cacheGet
  case muserKey of
    Just (CachedUserKey (Just userKey)) -> do
      egresos <- runDB $ selectList [EgresoUserId ==. userKey] ([Desc EgresoFecha] ++ limitFilter)
      sendStatusJSON ok200 egresos
    _ -> sendNotFound

postEgresosR :: Handler ()
postEgresosR = do
  msg <- requireCheckJsonBody :: Handler Egreso
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

getEgresoR :: Int64 -> Handler Value
getEgresoR n = do
  let key = toSqlKey n :: Key Egreso
  runDB (get key) >>= \case
    (Just v) -> sendStatusJSON ok200 v
    Nothing -> sendNotFound

deleteEgresoR :: Int64 -> Handler ()
deleteEgresoR n = do
  let k = toSqlKey n :: Key Egreso
  runDB $ deleteCascade $ k
  sendDeleted
