module Handler.Common where

import Crypto.BCrypt
import Data.Text.Encoding as TE
import Database.Persist.Sql (fromSqlKey, toSqlKey)
import Import

sendDeleted :: Handler a
sendDeleted = sendResponseStatus status200 ("Deleted" :: Text)

sendNotFound :: Handler a
sendNotFound = sendResponseStatus status404 ("Not Found" :: Text)

sendUnauthorized :: Handler a
sendUnauthorized = sendResponseStatus status401 ("Unauthorized" :: Text)

sendBadRequest :: Handler a
sendBadRequest = sendResponseStatus status400 ("Bad Request" :: Text)

hashText :: Text -> IO (Maybe ByteString)
hashText =
  (hashPasswordUsingPolicy slowerBcryptHashingPolicy) . TE.encodeUtf8
