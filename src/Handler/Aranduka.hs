module Handler.Aranduka where

import qualified Data.List as L
import Data.Text.Lazy.Builder as B
import qualified Database.Esqueleto as E
import Database.Esqueleto ((&&.), (==.), (?.), (^.), from, select, val, where_)
import Database.Esqueleto.Experimental as EX
import Database.Esqueleto.Internal.Sql as ES
import Database.Persist.Sql (fromSqlKey, rawSql, toSqlKey)
import Handler.Common
import Import

extractYear :: ES.UnsafeSqlFunctionArgument a => a -> E.SqlExpr (E.Value Int64)
extractYear = ES.unsafeSqlExtractSubField $ B.fromText ("YEAR" :: Text)

extractMonth :: ES.UnsafeSqlFunctionArgument a => a -> E.SqlExpr (E.Value Int64)
extractMonth = ES.unsafeSqlExtractSubField $ B.fromText ("MONTH" :: Text)

formatEgreso ::
  Int64 ->
  ( Entity Egreso,
    Entity User,
    Maybe (Entity TipoDocumentoEgreso),
    Maybe (Entity TipoIdentificacion),
    Maybe (Entity CondicionVenta),
    Maybe (Entity TipoEgreso),
    Maybe (Entity ClasificacionEgreso)
  ) ->
  Value
formatEgreso periodo (Entity k e, Entity _ u, Just (Entity _ td), Just (Entity _ ti), Just (Entity _ cv), Just (Entity _ te), Just (Entity _ ce)) =
  object
    [ "periodo" .= show periodo,
      "tipo" .= tipoDocumentoEgresoCodigo td,
      "relacionadoTipoIdentificacion" .= tipoIdentificacionCodigo ti,
      "fecha" .= show (egresoFecha e),
      "id" .= k,
      "ruc" .= userRuc u,
      "egresoMontoTotal" .= egresoMonto e,
      "relacionadoNombres" .= egresoNombre e,
      "relacionadoNumeroIdentificacion" .= egresoNumeroIdentificacion e,
      "timbradoCondicion" .= condicionVentaCodigo cv,
      "timbradoDocumento" .= egresoNumeroDocumento e,
      "timbradoNumero" .= egresoNumeroTimbrado e,
      "tipoEgreso" .= tipoEgresoCodigo te,
      "subtipoEgreso" .= clasificacionEgresoCodigo ce,
      "tipoTexto" .= tipoDocumentoEgresoNombre td
    ]
formatEgreso _ _ = object []

getEgresos :: Int64 -> Handler [Value]
getEgresos periodo = do
  egresos <- runDB $ select
    $ E.from
    $ \(e, u, td, ti, cv, te, ce) -> do
      where_
        ( e ^. EgresoUserId E.==. u ^. UserId
            &&. e ^. EgresoTipoDocumentoId E.==. td ?. TipoDocumentoEgresoId
            &&. E.not_ (E.isNothing (td ?. TipoDocumentoEgresoId))
            &&. e ^. EgresoTipoIdentificacionId E.==. ti ?. TipoIdentificacionId
            &&. E.not_ (E.isNothing (ti ?. TipoIdentificacionId))
            &&. e ^. EgresoCondicionVentaId E.==. cv ?. CondicionVentaId
            &&. E.not_ (E.isNothing (cv ?. CondicionVentaId))
            &&. e ^. EgresoTipoEgresoId E.==. te ?. TipoEgresoId
            &&. E.not_ (E.isNothing (te ?. TipoEgresoId))
            &&. e ^. EgresoClasificacionEgresoId E.==. ce ?. ClasificacionEgresoId
            &&. E.not_ (E.isNothing (ce ?. ClasificacionEgresoId))
            &&. extractYear (e ^. EgresoFecha) E.==. val periodo
        )
      return (e, u, td, ti, cv, te, ce)
  let egresosList = map (formatEgreso periodo) egresos
  return egresosList

formatIngreso ::
  Int64 ->
  ( Entity Ingreso,
    Entity User,
    Maybe (Entity TipoDocumentoIngreso),
    Maybe (Entity TipoIdentificacion),
    Maybe (Entity CondicionVenta),
    Maybe (Entity TipoIngreso)
  ) ->
  Value
formatIngreso periodo (Entity k i, Entity _ u, Just (Entity _ td), Just (Entity _ tid), Just (Entity _ cv), Just (Entity _ tin)) =
  object
    [ "periodo" .= show periodo,
      "tipo" .= tipoDocumentoIngresoCodigo td,
      "relacionadoTipoIdentificacion" .= tipoIdentificacionCodigo tid,
      "fecha" .= show (ingresoFecha i),
      "id" .= k,
      "ruc" .= userRuc u,
      "ingresoMontoGravado" .= ingresoGravado i,
      "ingresoMontoNoGravado" .= ingresoNoGravado i,
      "ingresoMontoTotal" .= ((ingresoGravado i) + (ingresoNoGravado i)),
      "relacionadoNombres" .= ingresoNombre i,
      "relacionadoNumeroIdentificacion" .= ingresoNumeroIdentificacion i,
      "timbradoCondicion" .= condicionVentaCodigo cv,
      "timbradoDocumento" .= ingresoNumeroDocumento i,
      "timbradoNumero" .= ingresoNumeroTimbrado i,
      "tipoIngreso" .= tipoIngresoCodigo tin,
      "tipoTexto" .= tipoDocumentoIngresoNombre td,
      "mes" .= (\(_, m, _) -> show m) (toGregorian (ingresoFecha i))
    ]
formatIngreso _ _ = object []

getIngresos :: Int64 -> Handler [Value]
getIngresos periodo = do
  ingresos <- runDB $ select
    $ E.from
    $ \(i, u, td, tid, cv, tin) -> do
      where_
        ( i ^. IngresoUserId E.==. u ^. UserId
            &&. i ^. IngresoTipoDocumentoId E.==. td ?. TipoDocumentoIngresoId
            &&. E.not_ (E.isNothing (td ?. TipoDocumentoIngresoId))
            &&. i ^. IngresoTipoIdentificacionId E.==. tid ?. TipoIdentificacionId
            &&. E.not_ (E.isNothing (tid ?. TipoIdentificacionId))
            &&. i ^. IngresoCondicionVentaId E.==. cv ?. CondicionVentaId
            &&. E.not_ (E.isNothing (cv ?. CondicionVentaId))
            &&. i ^. IngresoTipoIngresoId E.==. tin ?. TipoIngresoId
            &&. E.not_ (E.isNothing (tin ?. TipoIngresoId))
            &&. extractYear (i ^. IngresoFecha) E.==. val periodo
        )
      return (i, u, td, tid, cv, tin)
  let ingresosList = map (formatIngreso periodo) ingresos
  return ingresosList

getFamiliares :: Int64 -> Handler [Value]
getFamiliares _ = return []

getArandukaR :: Int64 -> Handler Value
getArandukaR periodo = do
  addHeader "Content-Disposition" "attachment; filename=\"response.json\""
  muserKey <- cacheGet
  case muserKey of
    Just (CachedUserKey (Just userKey)) -> do
      muser <- runDB $ get userKey
      case muser of
        Nothing -> do
          sendNotFound
        Just user -> do
          ingresos <- getIngresos periodo
          egresos <- getEgresos periodo
          familiares <- getFamiliares periodo
          sendStatusJSON ok200 $
            object
              [ "informante" .= object ["ruc" .= userRuc user],
                "identificacion" .= object ["periodo" .= show periodo],
                "ingresos" .= ingresos,
                "egresos" .= egresos,
                "familiares" .= familiares
              ]
    _ -> do
      sendNotFound

getPeriodos :: Key User -> Handler [Int64]
getPeriodos _ = do
  periodos <-
    runDB $ select $ EX.from $
      (SelectQuery $ E.from extractYearIngreso)
        `Union` (SelectQuery $ E.from extractYearEgreso)
  return $ map E.unValue $ periodos

extractYearIngreso :: SqlExpr (Entity Ingreso) -> SqlQuery (SqlExpr (E.Value Int64))
extractYearIngreso i = pure $ extractYear (i ^. IngresoFecha)

extractYearEgreso :: SqlExpr (Entity Egreso) -> SqlQuery (SqlExpr (E.Value Int64))
extractYearEgreso e = pure $ extractYear (e ^. EgresoFecha)

getPeriodosR :: Handler Value
getPeriodosR = do
  muserKey <- cacheGet
  case muserKey of
    Just (CachedUserKey (Just userKey)) -> do
      periodos <- getPeriodos userKey
      return $ object ["periodos" .= (L.sortBy (\a b -> compare b a) periodos)]
    _ -> do
      sendNotFound
